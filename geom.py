import json
import shapely
import fiona

def mm2pts(mm):
    return mm / 0.3528

def create_hole(width, radius, tip=0):
    line = shapely.geometry.LineString([(0, tip), (0, 0), (width, 0), (width, tip)])
    return line.buffer(radius / 2)


def read_json_polygon(fn):
    with open(fn, 'r') as json_file:
        json_dict = json.load(json_file)
        if json_dict['type'] == 'Polygon':
            return (json_dict['coordinates'])
        else:
            raise Exception('Whatever...')


def translate_polygon(polygon, new_extent):
    for coords in polygon:
        #print(new_extent)
        poly_extent = Extent()
        poly_extent.from_polygon(polygon)
        #print(poly_extent)

        scale_x = new_extent.width() / poly_extent.width()
        scale_y = new_extent.height() / poly_extent.height()
        dx = new_extent.minx - poly_extent.minx
        dy = new_extent.miny - poly_extent.miny
        #print(scale_x, dx)
        #print(scale_y, dy)

        result = []
        for coords in polygon:
            new_ring = []
            for coord in coords:
                print(coord[0], coord[1])
                new_x = ((coord[0] - poly_extent.minx) * scale_x) + new_extent.minx
                new_y = ((coord[1] - poly_extent.miny) * scale_y) + new_extent.miny
                new_coord = [new_x, new_y]
                #print(new_x, new_y)
                new_ring.append(new_coord)
            result.append(new_ring)
        return result


class Extent():
    def __init__(self, minx=0, miny=0, maxx=0, maxy=0):
        self.minx = minx
        self.miny = miny
        self.maxx = maxx
        self.maxy = maxy

    def __str__(self):
        return 'Extent[{0:.{6}f}, {1:.{6}f}, {2:.{6}f}, {3:.{6}f}, {4:.{6}f}, {5:.{6}f}]'.format(
            self.minx, self.miny,
            self.maxx, self.maxy,
            self.width(), self.height(),
            3
        )

    def width(self):
        return self.maxx - self.minx

    def height(self):
        return self.maxy - self.miny

    def from_polygon(self, polygon):
        self.minx = self.maxx = polygon[0][0][0]
        self.miny = self.maxy = polygon[0][0][1]

        for coords in polygon:
            for coord in coords:
                # print(coord[0], coord[1])
                self.minx = min(self.minx, coord[0])
                self.miny = min(self.miny, coord[1])
                self.maxx = max(self.maxx, coord[0])
                self.maxy = max(self.maxy, coord[1])

    def from_shapely(self, shape):
        bounds = shape.bounds
        self.minx = bounds[0]
        self.miny = bounds[1]
        self.maxx = bounds[2]
        self.maxy = bounds[3]


    def calculate_affine_parameters(self, other, stretch=False):
        scale_x = other.width() / self.width()
        scale_y = other.height() / self.height()
        #print('scales: {:2g}, {:2g}'.format(scale_x, scale_y))
        if not stretch:
            ratio = scale_x / scale_y
            abs_min_scale = min(abs(scale_x), abs(scale_y))
            #print('abs_min_scale:', abs_min_scale)
            #print('ratio', ratio)
            if abs(ratio) < 1: #
                if scale_y < 0:
                    scale_y = -abs_min_scale
                else:
                    scale_y = abs_min_scale
            else:
                if scale_x < 0:
                    scale_x = -abs_min_scale
                else:
                    scale_x = abs_min_scale
        #print('scales: {:2g}, {:2g}'.format(scale_x, scale_y))
        dx = other.minx - (self.minx * scale_x) + ((other.width() - (self.width() * scale_x)) / 2)
        dy = other.miny - (self.miny * scale_y) + ((other.height() - (self.height() * scale_y)) / 2)
        return [scale_x, 0, 0, scale_y, dx, dy]


if __name__ == '__main__':
    poly_rd = read_json_polygon('polygon_den_bosch.json')
    # print(poly_rd)

    paper_extent = Extent(0, 0, 10, 10)
    #print(paper_extent)
    poly_paper = translate_polygon(poly_rd, paper_extent)
    #print(len(poly_paper))
