import subprocess
import os
import csv

import gi
from gi.repository import Rsvg
import cairo
import fiona
from shapely.geometry import shape, LineString, Polygon
from shapely.affinity import affine_transform
from shapely.ops import transform
import shapely
import pyproj

import geom

def mm2pt(mm):
    return mm / 0.3528


def draw_geometry(context, shape, color=(0,0,0)):
    context.set_source_rgb(*color)  # line color green
    context.set_line_width(1)  # line width
    class_name = shape.__class__.__name__
    if class_name == 'Polygon':
        coords = shape.exterior.coords
        coord = coords[0]  # Get first coordinate
        context.move_to(coord[0], coord[1])
        # Loop all next coordinates:
        for coord in coords[1:]:
            context.line_to(coord[0], coord[1])  # Draw line
        context.stroke()
    elif class_name == 'MultiPolygon':
        for polygon in shape:
            coords = polygon.exterior.coords
            coord = coords[0]  # Get first coordinate
            context.move_to(coord[0], coord[1])
            # Loop all next coordinates:
            for coord in coords[1:]:
                context.line_to(coord[0], coord[1])  # Draw line
        context.stroke()
    else:
        print(f'draw_geometry() Unknown geometry class: {class_name}')

def write_text(context, txt, x, y, font_size, max_width, color=(0,0,0)):
    context.set_source_rgb(*color)  # set color blue
    context.set_font_size(font_size)
    context.select_font_face("Sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)

    extents = context.text_extents(txt)  # Calculate text bbox
    #print(extents.width)
    while extents.width > max_width:
        font_size -= 1
        context.set_font_size(font_size)
        extents = context.text_extents(txt)
        #print(extents.width)

    # Calculate ll-location of text bbox on canvas:
    text_x = x - (extents.width / 2 + extents.x_bearing)
    text_y = y - (extents.height / 2 + extents.y_bearing)

    context.move_to(text_x, text_y)
    context.text_path(txt)
    context.stroke()

def get_country(iso_a2):
    fn = 'data/countries.gpkg'
    wgs84 = pyproj.CRS('EPSG:4326')
    tm = pyproj.CRS('EPSG:3857')
    project = pyproj.Transformer.from_crs(wgs84, tm, always_xy=True).transform
    with fiona.open(fn, layer='countries') as src:
        #print(src.crs)
        for feat in src:
            #print(feat['properties']['iso_a2'])
            if feat['properties']['iso_a2'] == iso_a2:
                geom = shape(feat['geometry'])
                tm_geom = transform(project, geom)
                return tm_geom


def create_badges(svg_file_name, badge_width, badge_height, rows, columns):
    paper_width = columns * badge_width
    paper_height = rows * badge_height
    with cairo.SVGSurface(svg_file_name, paper_width, paper_height) as surface:
        context = cairo.Context(surface)
        draw_badge()
        return context

def draw_badge(context, badge_width, badge_height, margin, row, col, gem_geom, name, country):
        cut_color = (1, 0, 0)
        scan_color = (0, 0, 1)
        scan_color_2 = (0, 0, 0.5)

        ### Draw outline ###
        dx = col * badge_width
        dy = row * badge_height
        gem_extent = geom.Extent()
        gem_extent.from_shapely(gem_geom)
        #print(gem_extent)

        paper_extent = geom.Extent(margin + dx, (badge_height-margin) + dy, (badge_width-margin) + dx, margin + dy)
        #print(paper_extent)
        params = gem_extent.calculate_affine_parameters(paper_extent, stretch=False)
        gem_geom_paper = affine_transform(gem_geom, params)
        draw_geometry(context, gem_geom_paper, color=cut_color)

        ### Name ###
        x = (0.5 * badge_width) + dx
        y = (0.5 * badge_height) + dy

        write_text(context, name, x, y, mm2pt(12), mm2pt(100), color=scan_color)
        #write_text(context, visitor[3], 320, 260, 40, 400)

        ### Country ###
        country_geom = get_country(country)
        if country_geom is not None:
            country_extent = geom.Extent()
            country_extent.from_shapely(country_geom)
            #print(country_extent)

            x = (0.40 * badge_width) + dx
            y = (0.72 * badge_height) + dy

            sx = mm2pt(10)
            sy = mm2pt(10)

            paper_map_extent = geom.Extent(x-sx, y+sy, x+sx, y-sy)
            params = country_extent.calculate_affine_parameters(paper_map_extent, stretch=False)
            #print(params)
            geom_paper = affine_transform(country_geom, params)

            geom_paper_simple = geom_paper.simplify(0.2, preserve_topology=False)
            #print(geom_paper)

            draw_geometry(context, geom_paper_simple, color=scan_color)
            context.stroke()

        ### Hole ###
        w = mm2pt(8)
        r = mm2pt(3)
        t = mm2pt(1)
        hole = geom.create_hole(w, r, t)
        extent = geom.Extent()
        extent.from_shapely(hole)
        x = (0.49 * badge_width) + dx
        y = (0.2 * badge_height) + dy

        w = w + r
        h = r + t
        paper_extent = geom.Extent(x-(w/2), y+(h/2), x+(w/2), y-(h/2))
        params = extent.calculate_affine_parameters(paper_extent, stretch=False)
        #print(params)
        geom_paper = affine_transform(hole, params)

        draw_geometry(context, geom_paper, color=cut_color)

        ### Logo ###
        handle = Rsvg.Handle()
        svg = handle.new_from_file('/home/raymond/Dropbox/QGIS_Gebruikers_NL/events/202304_conf_and_hf/logo/logo_plain.svg')
        paper_rect = Rsvg.Rectangle() #100, 100, 20, 20)

        scale = 3

        x = ((0.535 * badge_width) + dx) / scale
        y = ((0.10 * badge_height) + dy) / scale

        matrix = cairo.Matrix(scale, 0, 0, scale, 0, 0)
        matrix.translate(x, y)
        context.transform(matrix)

        svg.render_cairo(context)

        matrix = cairo.Matrix((1/scale), 0, 0, (1/scale), 0, 0)
        #print(matrix)
        matrix.translate(-scale * x, -scale * y)
        #print(matrix)
        context.transform(matrix)


        ### Dinner ###
        if visitor[4] == 'yes':
            handle = Rsvg.Handle()
            svg = handle.new_from_file('data/dinner.svg')
            paper_rect = Rsvg.Rectangle() #100, 100, 20, 20)

            scale = 0.4

            x = ((0.22 * badge_width) + dx) / scale
            y = ((0.28 * badge_height) + dy) / scale

            matrix = cairo.Matrix(scale, 0, 0, scale, 0, 0)
            matrix.translate(x, y)
            context.transform(matrix)

            svg.render_cairo(context)

            matrix = cairo.Matrix((1/scale), 0, 0, (1/scale), 0, 0)
            #print(matrix)
            matrix.translate(-scale * x, -scale * y)
            #print(matrix)
            context.transform(matrix)


script_dir = os.path.dirname(os.path.realpath(__file__))
output_dir = os.path.join(script_dir, 'output')

visitors_csv_fn = 'data/badges.csv'

badge_width = mm2pt(140)  # badge width in pt
badge_height = badge_width / 1.80183541167543  # badge height in pt
margin = mm2pt(1)  # margin
rows = 3
columns = 2

with fiona.open("data/gemeente.gpkg") as gem_src:
    #print(gem_src.crs)
    for feat in gem_src:
        #print(feat)
        gem_geom = shape(feat['geometry'])

#gem_geom = Polygon([(1, 1), (2, 3), (3, 1)])

limit = 400
cnt = 0

svg_base_name = 'badges_20230419.svg'
svg_file_name = os.path.join(output_dir, svg_base_name)

paper_width = columns * badge_width
paper_height = rows * badge_height
with cairo.SVGSurface(svg_file_name, paper_width, paper_height) as surface:
    context = cairo.Context(surface)
    #print(context)

    row_num = 0
    col_num = 0
    with open(visitors_csv_fn, 'r') as visitors_csv:
        visitors_reader = csv.reader(visitors_csv)
        print(visitors_reader)
        #next(visitors_reader)  # Skip first row
        cnt = 0

        # Loop visitors:
        for visitor in visitors_reader:
            print(visitor)
            cnt += 1
            if cnt > limit or cnt > (rows * columns):
                break
            name = visitor[1] + ' '  + visitor[2]
            country = visitor[3]
            draw_badge(context, badge_width, badge_height, margin, row_num, col_num, gem_geom, name, country)

            row_num += 1
            if row_num == rows:
                row_num = 0
                col_num += 1

# Write to file
    context.stroke()

# Convert to eps
command = f'inkscape --export-type="eps" {svg_file_name}'  # command to be executed
print(command)
res = subprocess.call(command, shell=True)
print(res)

# Convert to eps
command = f'inkscape --export-type="dxf" {svg_file_name}'  # command to be executed
print(command)
res = subprocess.call(command, shell=True)
print(res)
